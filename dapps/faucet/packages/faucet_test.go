package faucet

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"github.com/iotaledger/hive.go/crypto/ed25519"
	"github.com/iotaledger/hive.go/identity"

	faucet "gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/dapps/faucet/packages/payload"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/dapps/valuetransfers/packages/address"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/packages/binary/messagelayer/message"
	data "gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/packages/binary/messagelayer/payload"
)

func TestIsFaucetReq(t *testing.T) {
	keyPair := ed25519.GenerateKeyPair()
	local := identity.NewLocalIdentity(keyPair.PublicKey, keyPair.PrivateKey)

	faucetPayload, err := faucet.New(address.Random(), 4)
	if err != nil {
		require.NoError(t, err)
		return
	}
	faucetMsg := message.New(
		message.EmptyId,
		message.EmptyId,
		time.Now(),
		local.PublicKey(),
		0,
		faucetPayload,
		0,
		ed25519.EmptySignature,
	)

	dataMsg := message.New(
		message.EmptyId,
		message.EmptyId,
		time.Now(),
		local.PublicKey(),
		0,
		data.NewData([]byte("data")),
		0,
		ed25519.EmptySignature,
	)

	assert.Equal(t, true, faucet.IsFaucetReq(faucetMsg))
	assert.Equal(t, false, faucet.IsFaucetReq(dataMsg))
}
