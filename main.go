package main

import (
	_ "net/http/pprof"

	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/pluginmgr/core"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/pluginmgr/research"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/pluginmgr/ui"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/pluginmgr/webapi"
	"github.com/iotaledger/hive.go/node"
)

func main() {
	node.Run(
		core.PLUGINS,
		research.PLUGINS,
		ui.PLUGINS,
		webapi.PLUGINS,
	)
}
