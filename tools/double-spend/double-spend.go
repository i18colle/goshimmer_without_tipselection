package main

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/client"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/dapps/valuetransfers/packages/address"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/dapps/valuetransfers/packages/address/signaturescheme"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/dapps/valuetransfers/packages/balance"
	valuepayload "gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/dapps/valuetransfers/packages/payload"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/dapps/valuetransfers/packages/transaction"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/dapps/valuetransfers/packages/wallet"
	"github.com/mr-tron/base58"
)

func main() {

	client := client.NewGoShimmerAPI("http://localhost:8080", http.Client{Timeout: 30 * time.Second})

	// genesis wallet
	genesisSeedBytes, err := base58.Decode("7R1itJx5hVuo9w9hjg5cwKFmek4HMSoBDgJZN8hKGxih")
	if err != nil {
		fmt.Println(err)
	}

	const genesisBalance = 1000000000
	genesisWallet := wallet.New(genesisSeedBytes)
	genesisAddr := genesisWallet.Seed().Address(0)
	genesisOutputID := transaction.NewOutputID(genesisAddr, transaction.GenesisID)

	// issue transactions which spend the same genesis output in all partitions
	conflictingTxs := make([]*transaction.Transaction, 2)
	conflictingTxIDs := make([]string, 2)
	receiverSeeds := make([]*wallet.Seed, 2)
	for i := range conflictingTxs {

		// create a new receiver wallet for the given conflict
		receiverSeeds[i] = wallet.NewSeed()
		destAddr := receiverSeeds[i].Address(0)

		tx := transaction.New(
			transaction.NewInputs(genesisOutputID),
			transaction.NewOutputs(map[address.Address][]*balance.Balance{
				destAddr: {
					{Value: genesisBalance, Color: balance.ColorIOTA},
				},
			}))
		tx = tx.Sign(signaturescheme.ED25519(*genesisWallet.Seed().KeyPair(0)))
		conflictingTxs[i] = tx

		valueObject := valuepayload.New(valuepayload.GenesisID, valuepayload.GenesisID, tx)

		// issue the value object
		txID, err := client.SendPayload(valueObject.Bytes())
		if err != nil {
			fmt.Println(err)
		}
		conflictingTxIDs[i] = txID
		fmt.Printf("issued conflict transaction %s\n", txID)
		//time.Sleep(7 * time.Second)
	}
}
