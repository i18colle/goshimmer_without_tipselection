package main

import (
	"fmt"

	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/dapps/valuetransfers/packages/wallet"
)

func main() {
	fmt.Println(wallet.New().Seed().Address(0))
}
