package webapi

import (
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/webapi"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/webapi/autopeering"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/webapi/data"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/webapi/drng"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/webapi/faucet"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/webapi/healthz"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/webapi/info"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/webapi/message"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/webapi/spammer"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/webapi/value"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/webauth"
	"github.com/iotaledger/hive.go/node"
)

var PLUGINS = node.Plugins(
	webapi.Plugin(),
	webauth.Plugin(),
	spammer.Plugin(),
	data.Plugin(),
	drng.Plugin(),
	faucet.Plugin(),
	healthz.Plugin(),
	message.Plugin(),
	autopeering.Plugin(),
	info.Plugin(),
	value.Plugin(),
)
