package research

import (
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/dapps/networkdelay"
	analysisclient "gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/analysis/client"
	analysisdashboard "gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/analysis/dashboard"
	analysisserver "gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/analysis/server"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/prometheus"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/remotelog"
	"github.com/iotaledger/hive.go/node"
)

var PLUGINS = node.Plugins(
	remotelog.Plugin(),
	analysisserver.Plugin(),
	analysisclient.Plugin(),
	analysisdashboard.Plugin(),
	prometheus.Plugin,
	networkdelay.App(),
)
