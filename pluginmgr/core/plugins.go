package core

import (
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/dapps/faucet"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/dapps/valuetransfers"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/autopeering"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/banner"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/bootstrap"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/cli"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/config"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/database"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/drng"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/gossip"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/gracefulshutdown"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/issuer"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/logger"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/messagelayer"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/metrics"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/portcheck"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/pow"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/profiling"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/sync"

	"github.com/iotaledger/hive.go/node"
)

var PLUGINS = node.Plugins(
	banner.Plugin(),
	config.Plugin(),
	logger.Plugin(),
	cli.Plugin(),
	gracefulshutdown.Plugin(),
	portcheck.Plugin(),
	profiling.Plugin(),
	database.Plugin(),
	autopeering.Plugin(),
	pow.Plugin,
	messagelayer.Plugin(),
	gossip.Plugin(),
	issuer.Plugin(),
	bootstrap.Plugin(),
	sync.Plugin(),
	metrics.Plugin(),
	drng.Plugin(),
	faucet.App(),
	valuetransfers.App(),
)
