package ui

import (
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/dashboard"
	"github.com/iotaledger/hive.go/node"
)

var PLUGINS = node.Plugins(
	dashboard.Plugin(),
)
