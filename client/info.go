package client

import (
	"net/http"

	webapi_info "gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/webapi/info"
)

const (
	routeInfo = "info"
)

// Info gets the info of the node.
func (api *GoShimmerAPI) Info() (*webapi_info.Response, error) {
	res := &webapi_info.Response{}
	if err := api.do(http.MethodGet, routeInfo, nil, res); err != nil {
		return nil, err
	}
	return res, nil
}
