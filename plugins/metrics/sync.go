package metrics

import (
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/packages/metrics"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/sync"
	"go.uber.org/atomic"
)

var (
	isSynced atomic.Bool
)

func measureSynced() {
	s := sync.Synced()
	metrics.Events().Synced.Trigger(s)
}

// Synced returns if the node is synced.
func Synced() bool {
	return isSynced.Load()
}
