package metrics

import (
	"sync"
	"testing"

	valuepayload "gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/dapps/valuetransfers/packages/payload"
	drngpayload "gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/packages/binary/drng/payload"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/packages/binary/messagelayer/payload"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/packages/metrics"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/messagelayer"
	"github.com/iotaledger/hive.go/events"
	"github.com/magiconair/properties/assert"
)

func TestMessageCountPerPayload(t *testing.T) {
	// it is empty initially
	assert.Equal(t, MessageTotalCount(), (uint64)(0))
	// simulate attaching 10 value payloads in 0s < t < 1s
	for i := 0; i < 10; i++ {
		increasePerPayloadCounter(valuepayload.Type)
	}
	assert.Equal(t, MessageTotalCount(), (uint64)(10))
	assert.Equal(t, MessageCountPerPayload(), map[payload.Type]uint64{valuepayload.Type: 10})
	// simulate attaching 5 drng payloads
	for i := 0; i < 5; i++ {
		increasePerPayloadCounter(drngpayload.Type)
	}
	assert.Equal(t, MessageTotalCount(), (uint64)(15))
	assert.Equal(t, MessageCountPerPayload(), map[payload.Type]uint64{valuepayload.Type: 10, drngpayload.Type: 5})
}

func TestMessageTips(t *testing.T) {
	var wg sync.WaitGroup
	// messagelayer TipSelector not configured here, so to avoid nil pointer panic, we instantiate it
	messagelayer.TipSelector()
	metrics.Events().MessageTips.Attach(events.NewClosure(func(tips uint64) {
		messageTips.Store(tips)
		wg.Done()
	}))
	wg.Add(1)
	measureMessageTips()
	wg.Wait()
	assert.Equal(t, MessageTips(), (uint64)(0))
}
