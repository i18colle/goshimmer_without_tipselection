package dashboard

import (
	"net/http"
	"sync"

	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/dapps/faucet"
	faucetpayload "gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/dapps/faucet/packages/payload"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/dapps/valuetransfers/packages/address"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/config"
	"gitlab.imt-atlantique.fr/i18colle/goshimmer_without_tipselection/plugins/messagelayer"

	"github.com/labstack/echo"
	"github.com/pkg/errors"
)

// ReqMsg defines the struct of the faucet request message ID.
type ReqMsg struct {
	ID string `json:"MsgId"`
}

func setupFaucetRoutes(routeGroup *echo.Group) {
	routeGroup.GET("/faucet/:hash", func(c echo.Context) (err error) {
		addr, err := address.FromBase58(c.Param("hash"))
		if err != nil {
			return errors.Wrapf(ErrInvalidParameter, "faucet request address invalid: %s", addr)
		}

		t, err := sendFaucetReq(addr)
		if err != nil {
			return
		}

		return c.JSON(http.StatusOK, t)
	})
}

var fundingReqMu = sync.Mutex{}

func sendFaucetReq(addr address.Address) (res *ReqMsg, err error) {
	fundingReqMu.Lock()
	defer fundingReqMu.Unlock()
	faucetPayload, err := faucetpayload.New(addr, config.Node().GetInt(faucet.CfgFaucetPoWDifficulty))
	if err != nil {
		return nil, err
	}
	msg, err := messagelayer.MessageFactory().IssuePayload(faucetPayload)
	if err != nil {
		return nil, errors.Wrapf(ErrInternalError, "Failed to send faucet request: %s", err.Error())
	}

	r := &ReqMsg{
		ID: msg.Id().String(),
	}
	return r, nil
}
